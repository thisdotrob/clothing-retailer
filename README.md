# Clothing retailer
[![Build Status](https://travis-ci.org/thisdotrob/clothing-retailer.svg?branch=master)](https://travis-ci.org/thisdotrob/clothing-retailer) [![Coverage Status](https://coveralls.io/repos/github/thisdotrob/clothing-retailer/badge.svg?branch=master)](https://coveralls.io/github/thisdotrob/clothing-retailer?branch=master)

- [Setup](#setup)
- [Running tests](#running-tests)
- [Usage](#usage)

## Setup
0. Install [Ruby](ruby) and [Bundler](bundler)
0. Clone this repo: `git@bitbucket.org:thisdotrob/clothing-retailer.git`
0. Run `bundle install`

## Running tests
0. For a single test run, use `rspec`
0. For continuous testing, use `bundle exec guard`

## Usage
0. Run `ruby app.rb`
0. Visit [http://localhost:4567](http://localhost:4567)
