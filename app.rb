require 'sinatra/base'
require_relative './lib/shop'

class Bsos < Sinatra::Base
  enable :sessions

  def show_products
    category = session[:category] || :footwear
    shop = (session[:shop] ||= Shop.new)
    @products = shop.products(category)
    @gender = shop.gender.capitalize
    erb(:products)
  end

  get '/' do
    show_products
  end

  get '/category/:category' do
    session[:category] = params[:category].to_sym
    show_products
  end

  get '/gender/:gender' do
    session[:shop].set_gender(params[:gender].to_sym)
    show_products
  end

  # get '/formalwear' do
  #   session[:category] = :formalwear
  #   show_products
  # end
  #
  # get '/footwear' do
  #   session[:category] = :footwear
  #   show_products
  # end
  #
  # get '/casualwear' do
  #   session[:category] = :casualwear
  #   show_products
  # end

  # start the server if ruby file executed directly
  run! if app_file == $0
end
