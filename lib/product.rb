class Product

  attr_reader :name, :price, :discounted_price, :stock

  def initialize(args)
    @name = args[:name]
    @price = args[:price].to_f
    @discounted_price = (args[:discounted_price] || @price).to_f
    @stock = args[:stock].to_i
  end

end
