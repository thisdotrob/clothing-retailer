require_relative 'product'
require 'json'

class ProductList
  attr_reader :gender

  def initialize(args = {})
    @product_class = args[:product_class] || Product
    filename = args[:filename] || 'products.json'
    parse_products(filename)
    @gender = :mens
  end

  def set_gender(gender)
    @gender = (gender == :womens ? gender : :mens)
  end

  def footwear
    @footwear.dup[@gender]
  end

  def casualwear
    @casualwear.dup[@gender]
  end

  def formalwear
    @formalwear.dup[@gender]
  end

  private

  def parse_products(filename)
    products = JSON.parse(File.read(filename), symbolize_names: true)
    @footwear = create_products(products[:footwear])
    @casualwear = create_products(products[:casualwear])
    @formalwear = create_products(products[:formalwear])
  end

  def create_products(products_hash)
    { mens:   initialize_products(products_hash[:mens]),
      womens: initialize_products(products_hash[:womens]) }
  end

  def initialize_products(products_array)
    products_array.map { |product_hash| @product_class.new(product_hash) }
  end
end
