require_relative 'product_list'

class Shop
  def initialize(product_list_class = ProductList)
    @product_list = product_list_class.new
  end

  def products(type)
    case type
    when :footwear then @product_list.footwear
    when :casualwear then @product_list.casualwear
    when :formalwear then @product_list.formalwear
    else @product_list.all
    end
  end

  def set_gender(gender)
    @product_list.set_gender(gender)
  end

  def gender
    @product_list.gender
  end

end
