require 'product_list'

describe ProductList do
  let(:product){ double('product') }
  let(:product_class){ double('product_class', new: product) }
  let(:filename){ 'spec/fixtures/products.json' }
  subject{ ProductList.new(filename: filename, product_class: product_class) }

  describe '#initialize' do
    it 'should parse the products from the json file' do
      expect(product_class).to receive(:new).exactly(13).times
      subject
    end
  end

  describe '#set_gender' do
    it 'should set the gender to \'womens\'' do
      subject.set_gender(:womens)
      expect(subject.gender).to eq(:womens)
    end

    it 'should default to \'mens\'' do
      subject.set_gender(:invalid)
      expect(subject.gender).to eq(:mens)
    end
  end

  describe '#footwear' do
    it 'should return mens footwear' do
      expect(subject.footwear).to eq([product, product, product])
    end

    it 'should return womens footwear' do
      subject.set_gender(:womens)
      expect(subject.casualwear).to eq([product, product])
    end
  end

  describe '#casualwear' do
    it 'should return mens casualwear' do
      expect(subject.casualwear).to eq([product, product])
    end
  end

  describe '#formalwear' do
    it 'should return mens formalwear' do
      expect(subject.formalwear).to eq([product, product])
    end
  end

  describe '#gender' do
    it 'should return the current gender' do
      expect(subject.gender).to eq(:mens)
    end
  end

end
