feature 'Displaying products' do
  scenario 'Men\'s footwear is displayed by default' do
    visit('/')
    expect(page).to have_content('Leather Driver Saddle Loafers, Tan')
    expect(page).to have_content('Flip Flops, Red')
    expect(page).to have_content('Flip Flops, Blue')
  end

  scenario 'Switching genders' do
    visit('/')
    click_link('Mens')
    expect(page).to_not have_content('Leather Driver Saddle Loafers, Tan')
    expect(page).to have_content('Almond Toe Court Shoes, Patent Black')
  end

  scenario 'Displaying women\'s formalwear' do
    visit('/')
    click_link('Mens')
    click_link('Formalwear')
    expect(page).to_not have_content('Almond Toe Court Shoes, Patent Black')
    expect(page).to have_content('Bird Print Dress, Black')
  end

end
