require 'shop'

describe Shop do
  let(:product_list){ double('product_list') }
  let(:product_list_class){ double('product_list_class', new: product_list) }

  subject{ Shop.new(product_list_class) }

  describe '#initialize' do
    it 'retrieves the products from the .json' do
      expect(product_list_class).to receive(:new)
      subject
    end
  end

  describe '#products' do
    it 'lists all the products' do
      expect(product_list).to receive(:all)
      subject.products('')
    end

    it 'lists footwear products' do
      expect(product_list).to receive(:footwear)
      subject.products(:footwear)
    end

    it 'lists casualwear products' do
      expect(product_list).to receive(:casualwear)
      subject.products(:casualwear)
    end

    it 'lists formalwear products' do
      expect(product_list).to receive(:formalwear)
      subject.products(:formalwear)
    end
  end

  describe '#set_gender' do
    it 'switches genders' do
      expect(product_list).to receive(:set_gender).with(:gender)
      subject.set_gender(:gender)
    end
  end

  describe '#gender' do
    it 'should return the gender currently being displayed' do
      expect(product_list).to receive(:gender)
      subject.gender
    end
  end

end
