require 'product'

describe Product do
  let(:args){ { name:  "Fine Stripe Short Sleeve Shirt, Green",
                price: "49.99",
                discounted_price: "39.99",
                stock: "3" } }
  subject{ Product.new(args) }

  describe '#name' do
    it 'should return the name of the product' do
      expect(subject.name).to eq(args[:name])
    end
  end

  describe '#price' do
    it 'should return the price of the product' do
      expect(subject.price).to eq(args[:price].to_f)
    end
  end

  describe '#stock' do
    it 'should return the stock of the product' do
      expect(subject.stock).to eq(args[:stock].to_i)
    end
  end

  describe '#discounted_price' do
    context 'when a discounted price exists' do
      it 'should return the discounted price of the product' do
        expect(subject.discounted_price).to eq(args[:discounted_price].to_f)
      end
    end
    context 'when there is no discounted price' do
      it 'should return the standard price of the product' do
        modified_args = { name:  "Fine Stripe Short Sleeve Shirt, Green",
                          price: "49.99",
                          stock: "3" }
        modified_subject = Product.new(modified_args)
        expect(modified_subject.discounted_price).to eq(args[:price].to_f)
      end
    end
  end

end
