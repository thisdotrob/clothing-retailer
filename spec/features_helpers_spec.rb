module FeaturesHelpers
  def first_product_container
    first('//div[contains(@class, \'product-container\')]')
  end

  def first_product_name
    first('//h4[contains(@class, \'product-name\')]')
  end
  
end
